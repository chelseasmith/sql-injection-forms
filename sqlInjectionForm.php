<?php

?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>SQL Injection Form</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <script src="js/bootstrap.js"></script>
</head>
<body>

<form class="form">
<fieldset>

<!-- Form Name -->

<div class="container">
    <div class="form-current">
        <h2>Resume Uploader: Sql Injection</h2>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="firstName"></label>
  <input id="firstName" name="firstName" type="text" placeholder="First Name" class="form-control input-md" required="">

  </div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="lastName"></label>
  <input id="lastName" name="lastName" type="text" placeholder="Last Name" class="form-control input-md" required="">

  </div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email"></label>
  <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md">

  </div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="comment"></label>
    <textarea class="form-control" id="comment" name="comment" placeholder="Comment"></textarea>
  </div>

<!-- File Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="filebutton"></label>
    <input id="filebutton" name="filebutton" class="input-file" type="file">
  </div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>

</div>
</div>
</fieldset>
</form>
</body>
</html>